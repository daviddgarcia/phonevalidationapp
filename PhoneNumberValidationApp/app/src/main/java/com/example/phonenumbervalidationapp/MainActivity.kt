package com.example.phonenumbervalidationapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.phonenumbervalidationapp.API.retrofitServices
import com.example.phonenumbervalidationapp.databinding.ActivityMainBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    var allLanguages = emptyList<Language>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initListener()
        getLanguages()
    }

    private fun initListener() {
        binding.btnValidateNumber.setOnClickListener{
            val texto:String = binding.edtDescription.text.toString()

            if (texto.isNotEmpty())
                showLoading()
                getTextLanguages(texto)
        }
    }

    private fun getTextLanguages(texto: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val result: Response<DetectionResponse> = retrofitServices.getTextLanguage(texto)

            if (result.isSuccessful){
                checkResult(result.body())
            }else{
                showError()
            }
            cleantext()
            hideLoading()
        }
    }

    private fun hideLoading() {
        runOnUiThread {
            binding.progressbar.visibility=View.GONE
        }
    }

    private fun cleantext() {
        binding.edtDescription.setText("")
    }

    private fun checkResult(detectionResponse: DetectionResponse?) {
        if(detectionResponse != null && !detectionResponse.data.detections.isNullOrEmpty()){
            val correctLanguages: List<Detection> = detectionResponse.data.detections.filter { it.isReliable }

            if (correctLanguages.isNotEmpty()){
                val languageName: Language? = allLanguages.find { it.code == correctLanguages.first().language }

                if (languageName != null){
                    runOnUiThread{
                        Toast.makeText(this,
                        "El idioma es: ${languageName.name}",
                        Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }

    private fun showLoading() {
        binding.progressbar.visibility = View.VISIBLE
    }

    //obtener el listado
    private fun getLanguages(){
        CoroutineScope(Dispatchers.IO).launch{
            var languages: Response<List<Language>> = retrofitServices.getLanguages()
            if (languages.isSuccessful){
                allLanguages = languages.body() ?: emptyList()
                showSucces()
            }else{
                showError()
            }

        }
    }
    private fun showError(){
        runOnUiThread{
            Toast.makeText(this,"Error al hacer la llamada",Toast.LENGTH_LONG).show()
        }
    }

    private fun showSucces() {
        runOnUiThread{
            Toast.makeText(this,"Peticion correcta",Toast.LENGTH_LONG).show()
        }

    }

}